<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\AppController;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends AppController{
    use RegistersUsers;

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['bail', 'required', 'string', 'min:3', 'max:50'],
            'email' => ['bail', 'required', 'email', 'unique:users,email'],
            'mobile' => ['bail', 'required', 'digits:10', 'unique:users,mobile'],
            'password' => ['bail', 'required', 'string', 'min:8', 'max:64'],
        ], [
            'name.required' => 'We need your name to sign you up.',
            'name.min' => 'Your name must be between 3 to 50 characters.',
            'name.max' => 'Your name must be between 3 to 50 characters.',
            'email.required' => 'We need your email to sign you up.',
            'email.email' => 'Your email address must be in an acceptable format.',
            'email.unique' => 'We already have an account with that email.',
            'mobile.required' => 'We need your mobile number to sign you up.',
            'mobile.digits' => 'Your mobile number must be exactly 10 digits.',
            'mobile.unique' => 'We already have an account with that mobile.',
            'password.required' => 'A password is required to secure your account.',
            'password.min' => 'Password must be 8 to 64 alphanumeric characters.',
            'password.max' => 'Password must be 8 to 64 alphanumeric characters.'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'data' => [
                    'created' => false,
                    'message' => $validator->errors()->first()
                ]
            ], 400);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json([
            'data' => [
                'created' => true,
                'message' => 'You have been successfully registered.'
            ]
        ], 201);
    }

    public function emailExists(Request $request){
        $email = $request->email;
        $validator = Validator::make($request->all(), ['email' => ['required', 'email']]);
        if ($validator->fails()) {
            return response()->json([
                'data' => [
                    'invalid' => true,
                    'found' => false
                ]
            ], 200);
        }

        try {
            $user = User::where('email', $email)->firstOrFail();
            return response()->json([
                'data' => [
                    'invalid' => false,
                    'found' => true,
                ]
            ], 200);
        }
        catch (ModelNotFoundException $exception) {
            return response()->json([
                'data' => [
                    'invalid' => false,
                    'found' => false,
                ]
            ], 200);
        }
    }

    public function mobileExists(Request $request){
        $mobile = $request->mobile;
        $validator = Validator::make($request->all(), ['mobile' => ['required', 'digits:10']]);
        if ($validator->fails()) {
            return response()->json([
                'data' => [
                    'invalid' => true,
                    'found' => false
                ]
            ], 200);
        }

        try {
            $user = User::where('mobile', $mobile)->firstOrFail();
            return response()->json([
                'data' => [
                    'invalid' => false,
                    'found' => true,
                ]
            ], 200);
        }
        catch (ModelNotFoundException $exception) {
            return response()->json([
                'data' => [
                    'invalid' => false,
                    'found' => false,
                ]
            ], 200);
        }
    }
}
