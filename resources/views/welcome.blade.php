<!doctype html>
<html lang = "en-US">
<head>
    <meta charset = "utf-8">
    <meta name = "viewport" content = "width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href = "https://fonts.googleapis.com/css?family=Nunito:200,600" rel = "stylesheet">

    <!-- FontAwesome Iconsheet -->
    <link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Axios JS -->
    <script src = "https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- jQuery JS -->
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

    <!-- Bootstrap CSS -->
    <link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity = "sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin = "anonymous">

    <!-- JavaScript -->
    <script src = "//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/alertify.min.js"></script>

    <!-- CSS -->
    <link rel = "stylesheet" href = "//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/alertify.min.css"/>

    <!-- Default theme -->
    <link rel = "stylesheet" href = "//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/default.min.css"/>

    <!-- Bootstrap theme -->
    <link rel = "stylesheet" href = "//cdn.jsdelivr.net/npm/alertifyjs@1.11.4/build/css/themes/bootstrap.min.css"/>

    <style>

        @import url('https://fonts.googleapis.com/css?family=ABeeZee&display=swap');

        * {
            font-family: "AbeeZee", sans-serif;
        }

        .border-card-light {
            border-color: rgba(164, 164, 164, 0.65);
        }

        .ajs-header {
            background-color: white !important;
            border-bottom: none !important;
            border-radius: 15px 15px 0 0 !important;
            font-weight: normal !important;
            font-family: "AbeeZee", sans-serif !important;
            color: #007bff !important;
            font-size: 21px !important;
        }

        .ajs-footer {
            background-color: white !important;
            border-top: none !important;
            border-radius: 0 0 15px 15px !important;
            font-weight: normal !important;
            font-family: "AbeeZee", sans-serif !important;
        }

        .ajs-content {
            padding-left: 0 !important;
        }

        .ajs-dialog {
            border-radius: 15px !important;
        }

        .alertify .ajs-modal {
            top: 25%;
        }
    </style>
</head>
<body>
<div class = "container-fluid">
    <div class = "row" style = "margin-top: 20vh;">
        <div class = "mx-auto bg-white shadow-sm border-card-light border p-3" style = "min-width: 480px; border-radius: 15px;">
            <h5 class = "text-primary">Wanderlust - Sign Up</h5>
            <hr>
            <form class = "mt-3" id = "registrationForm" data-parsley-validate>
                <div class = "form-group">
                    <label for = "name">Name</label>
                    <input id = "name" type = "text" class = "form-control" placeholder = "What's your name?">
                    <div class = "valid-feedback">
                        Looks good
                    </div>
                    <div class = "invalid-feedback">
                        Doesn't look right
                    </div>
                </div>
                <div class = "form-group">
                    <label for = "email">Email</label>
                    <input id = "email" type = "email" class = "form-control" placeholder = "What's your email?">
                    <div class = "valid-feedback">
                        Available
                    </div>
                    <div class = "invalid-feedback">
                        Unavailable or invalid
                    </div>
                </div>
                <div class = "form-group">
                    <label for = "mobile">Mobile</label>
                    <input id = "mobile" type = "text" class = "form-control" placeholder = "What's your mobile number?">
                    <div class = "valid-feedback">
                        Available
                    </div>
                    <div class = "invalid-feedback">
                        Unavailable or invalid
                    </div>
                </div>
                <div class = "form-group">
                    <label for = "password">Password</label>
                    <input id = "password" type = "password" class = "form-control" placeholder = "Type a good password">
                    <div class = "valid-feedback">
                        Looks good
                    </div>
                    <div class = "invalid-feedback">
                        Doesn't look right
                    </div>
                </div>
                <div class = "progress" id = "progressBar" style = "opacity: 0;">
                    <div class = "progress-bar progress-bar-striped progress-bar-animated" role = "progressbar" aria-valuenow = "100" aria-valuemin = "0" aria-valuemax = "100" style = "width: 100%"></div>
                </div>
                <div class = "row mt-3" id = "buttons">
                    <div class = "col-6">
                        <button type = "button" onclick = "validateAndSubmit();" class = "btn btn-outline-primary float-right w-100 shadow-sm">
                            Sign
                            Up
                        </button>
                    </div>
                    <div class = "col-6">
                        <button type = "button" onclick = "invokeGoogleSignUp();" class = "btn btn-outline-danger float-right w-100 shadow-sm" title = "Sign up with Google">
                            <i class = "fa fa-google"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script>
	alertify.defaults = {
		autoReset: false,
		maximizable: false,
		modal: true,
		movable: false,
		transition: 'zoom',

		glossary: {
			title: 'WanderLust',
			ok: 'Okay',
			cancel: 'Cancel'
		},

		theme: {
			input: 'ajs-input',
			ok: 'ajs-button-new btn btn-primary',
			cancel: 'ajs-button-new btn btn-danger'
		}
	};

	const ax = axios.create({
		baseURL: 'http://localhost:8000/api/',
		timeout: 1000,
	});
	const name = 0;
	const email = 1;
	const mobile = 2;
	const password = 3;

	let allValid = false;
	let validations = [false, false, false, false];

	$(document).ready(function () {
		alertify.alert().set({
			onshow: function () {
				$('.ajs-button-new').removeClass('ajs-button ajs-button-new');
			}
		});

		$('#email').on('change', function () {
			ax.post('/users/email', {
				'email': $('#email').val()
			}).then(function (response) {
				const result = response.data.data.found;
				const invalid = response.data.data.invalid;
				if (!result && !invalid) {
					updateValidations(email, true);
					$('#email').removeClass('is-invalid');
					$('#email').addClass('is-valid');
				} else {
					updateValidations(email, false);
					$('#email').removeClass('is-valid');
					$('#email').addClass('is-invalid');
				}
			}).catch(function (error) {

			});
		});

		$('#mobile').on('change', function () {
			ax.post('/users/mobile', {
				'mobile': $('#mobile').val()
			}).then(function (response) {
				const result = response.data.data.found;
				const invalid = response.data.data.invalid;
				if (!result && !invalid) {
					updateValidations(mobile, true);
					$('#mobile').removeClass('is-invalid');
					$('#mobile').addClass('is-valid');
				} else {
					updateValidations(mobile, false);
					$('#mobile').removeClass('is-valid');
					$('#mobile').addClass('is-invalid');
				}
			}).catch(function (error) {

			});
		});

		$('#name').on('change keyup', function () {
			const invalid = isNameValid($('#name').val());
			if (invalid) {
				updateValidations(name, true);
				$('#name').removeClass('is-invalid');
				$('#name').addClass('is-valid');
			} else {
				updateValidations(name, false);
				$('#name').removeClass('is-valid');
				$('#name').addClass('is-invalid');
			}
		});

		$('#password').on('change keyup', function () {
			const invalid = isPasswordValid($('#password').val());
			if (invalid) {
				updateValidations(password, true);
				$('#password').removeClass('is-invalid');
				$('#password').addClass('is-valid');
			} else {
				updateValidations(password, false);
				$('#password').removeClass('is-valid');
				$('#password').addClass('is-invalid');
			}
		});
	});

	function validateAndSubmit() {
		if (allValid) {
			showProgress(true);
			ax.post('/users/register', {
				'name': $('#name').val(),
				'mobile': $('#mobile').val(),
				'email': $('#email').val(),
				'password': $('#password').val()
			}).then(function (response) {
				showProgress(false);
				$('#registrationForm').trigger('reset');
				$('#name').removeClass('is-valid');
				$('#email').removeClass('is-valid');
				$('#mobile').removeClass('is-valid');
				$('#password').removeClass('is-valid');
				validations[name] = false;
				validations[email] = false;
				validations[mobile] = false;
				validations[password] = false;
				allValid = false;
				alertify.alert(response.data.data.message);
			}).catch(function (error) {
				showProgress(false);
				alertify.alert(error.response.data.data.message);
			});
		} else {
			incompleteFormDialog();
		}
	}

	function invokeGoogleSignUp() {

	}

	function incompleteFormDialog() {
		alertify.alert('Please fill out all the fields properly to continue.');
	}

	function showProgress(show) {
		if (show) {
			$('#buttons').hide();
			$('#progressBar').animate({'opacity': 1}, 'fast');
		} else {
			$('#progressBar').animate({'opacity': 0}, 'fast');
			$('#buttons').show();
		}
	}

	function isNameValid(name) {
		let n = name.trim();
		return n.length >= 3 && n.length < 50;
	}

	function isPasswordValid(password) {
		return password.length >= 8 && password.length < 64;
	}

	function updateValidations(type, value) {
		validations[type] = value;
		allValid = validations[name] && validations[email] && validations[mobile] && validations[password];
	}
</script>
</html>
